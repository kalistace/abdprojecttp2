package models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class AcroUIModel {

	private String motAAcrosticher;

	private Map<Character, List<String>> lettre_acrostiches_Map;

	private final List<ChangeListener> listeVues;

	public AcroUIModel() {
		motAAcrosticher = "";
		lettre_acrostiches_Map = new HashMap<Character, List<String>>();
		this.listeVues = new ArrayList<ChangeListener>();
	}

	public void setMotAAcrosticher(String motAAcrosticher) {
		this.motAAcrosticher = motAAcrosticher;
	}

	public Map<Character, List<String>> getLettre_acrostiches_Map() {
		return new HashMap<Character, List<String>>(lettre_acrostiches_Map);
	}

	public void setLettre_acrostiches_Map(
			Map<Character, List<String>> lettre_acrostiches_Map) {
		this.lettre_acrostiches_Map = lettre_acrostiches_Map;
		this.traiterEvent(new ChangeEvent(this));
	}

	public String getMotAAcrosticher() {
		return motAAcrosticher;
	}

	public final synchronized void addChangeListener(final ChangeListener chl) {
		if (!this.listeVues.contains(chl)) {
			this.listeVues.add(chl);
		}
	}

	public final synchronized void removeChangeListener(final ChangeListener chl) {
		if (this.listeVues.contains(chl)) {
			this.listeVues.remove(chl);
		}
	}

	protected final synchronized void traiterEvent(final ChangeEvent e) {
		for (final ChangeListener listener : this.listeVues) {
			listener.stateChanged(e);
		}
	}

	public List<String> getAutres(char lettre) {
		return this.lettre_acrostiches_Map.get(lettre);
	}

	// L'element selectionn� est toujours le premier de la liste
	public void notifyAutreSelected(String lettre, String selectedAutre,
			String current) {
		List<String> listForLettre = this.lettre_acrostiches_Map.get(lettre
				.toCharArray()[0]);
		int indexOfCurrent = listForLettre.indexOf(current);
		int indexOfAutre = listForLettre.indexOf(selectedAutre);
		Collections.swap(listForLettre, indexOfCurrent, indexOfAutre);
	}

	public List<String> getAcrosticheAAfficher() {
		List<String> listMotAAfficher = new ArrayList<String>();
		String copyMotAAcrosticher = new String(motAAcrosticher);
		int position = copyMotAAcrosticher.length();
		for (int i = position; i > 0; i--) {
			char key = copyMotAAcrosticher.charAt(i - 1);
			int numberOfSameLetter = utils.StringUtils.countOccurrences(
					copyMotAAcrosticher, key);
			String motToReturn = lettre_acrostiches_Map.get(key).get(
					numberOfSameLetter-1);// les liste sont triées par ordre
										// d'occurence d'une lettre dans un mot
			copyMotAAcrosticher = copyMotAAcrosticher.substring(0, i - 1);
			listMotAAfficher.add(motToReturn);
		}
		Collections.reverse(listMotAAfficher);
		return listMotAAfficher;
	}
}
