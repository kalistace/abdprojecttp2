package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class AcrosticheDaoImpl implements AcrosticheDAO {

	Connection con;

	public AcrosticheDaoImpl() throws SQLException {
		con = DriverManager
				.getConnection("jdbc:oracle:thin:CSD/grizou547@localhost:1521:orcl");
	}

	private Map<Character, List<String>> getAcrosticheExistant(
			String stringAAcrosticher) throws SQLException {
		PreparedStatement selectAnExistingAcrostiche = null;
		Map<Character, List<String>> lettre_acrostiches_Map = new HashMap<Character, List<String>>();

		String selectAnExistingAcrosticheString = "select am.motLettre from acrostiches a, acrotiches_mots am where a.ID = am.IDAcrostiches and a.motComplet = ? order by am.position";
		try {
			con.setAutoCommit(false);
			selectAnExistingAcrostiche = con
					.prepareStatement(selectAnExistingAcrosticheString);
			selectAnExistingAcrostiche.setString(1, stringAAcrosticher);
			ResultSet resultQuery = selectAnExistingAcrostiche.executeQuery();
			con.commit();
			while (resultQuery.next()) {
				String name = resultQuery.getString(1);

				char firstLetter = name.charAt(0);
				if (!lettre_acrostiches_Map.containsKey(firstLetter)) {
					List<String> listeStringNew = new ArrayList<String>();
					listeStringNew.add(name);
					lettre_acrostiches_Map.put(firstLetter, listeStringNew);
				} else {
					List<String> listeStringModify = lettre_acrostiches_Map
							.get(firstLetter);
					listeStringModify.add(name);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			if (con != null) {
				try {
					System.err.print("Transaction is being rolled back");
					con.rollback();
				} catch (SQLException excep) {
					e.printStackTrace();
				}
			}
		} finally {
			if (selectAnExistingAcrostiche != null) {
				selectAnExistingAcrostiche.close();
			}
			con.setAutoCommit(true);
		}
		return lettre_acrostiches_Map;
	}

	private Map<Character, List<String>> acrosticherNouveauMot(
			String stringAAcrosticher) throws SQLException {
		PreparedStatement selectAllAcrosticheFromAWord = null;
		Map<Character, List<String>> lettre_acrostiches_Map = new HashMap<Character, List<String>>();

		StringBuffer selectAllAcrosticheFromAWordStringBuffer = new StringBuffer();
		selectAllAcrosticheFromAWordStringBuffer
				.append("select mot from admots where qh = 1 and typeGram like 'adj.' and ( mot like ? ");
		for (int i = 0; i < stringAAcrosticher.length() - 1; i++) {
			selectAllAcrosticheFromAWordStringBuffer.append("or mot like ?");
		}
		selectAllAcrosticheFromAWordStringBuffer.append(" )");

		String selectAllAcrosticheFromAWordString = selectAllAcrosticheFromAWordStringBuffer
				.toString();
		try {
			con.setAutoCommit(false);
			selectAllAcrosticheFromAWord = con
					.prepareStatement(selectAllAcrosticheFromAWordString);
			for (int i = 0; i < stringAAcrosticher.length(); i++) {
				selectAllAcrosticheFromAWord.setString(i + 1,
						stringAAcrosticher.charAt(i) + "%");
			}
			ResultSet resultQuery = selectAllAcrosticheFromAWord.executeQuery();
			con.commit();
			while (resultQuery.next()) {
				String name = resultQuery.getString(1);
				char firstLetter = name.charAt(0);
				if (!lettre_acrostiches_Map.containsKey(firstLetter)) {
					List<String> listeStringNew = new ArrayList<String>();
					listeStringNew.add(name);
					lettre_acrostiches_Map.put(firstLetter, listeStringNew);
				} else {
					List<String> listeStringModify = lettre_acrostiches_Map
							.get(firstLetter);
					listeStringModify.add(name);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			if (con != null) {
				try {
					System.err.print("Transaction is being rolled back");
					con.rollback();
				} catch (SQLException excep) {
					e.printStackTrace();
				}
			}
		} finally {
			if (selectAllAcrosticheFromAWord != null) {
				selectAllAcrosticheFromAWord.close();
			}
			con.setAutoCommit(true);
		}
		return lettre_acrostiches_Map;
	}

	@Override
	public void changeHQ(String id, int value) throws SQLException {
		PreparedStatement updateHQ = null;

		String updateHQString = "update adStrings set qh = ? where rowid = ?";
		try {
			con.setAutoCommit(false);
			updateHQ = con.prepareStatement(updateHQString);
			updateHQ.setInt(1, value);
			updateHQ.setString(2, id);
			updateHQ.executeUpdate();
			con.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			if (con != null) {
				try {
					System.err.print("Transaction is being rolled back");
					con.rollback();
				} catch (SQLException excep) {
					e.printStackTrace();
				}
			}
		} finally {
			if (updateHQ != null) {
				updateHQ.close();
			}
			con.setAutoCommit(true);
		}

	}

	@Override
	public Map<Character, List<String>> acrosticher(String StringAAcrosticher)
			throws SQLException {
		Map<Character, List<String>> lettre_acrostiches_MapExistant = getAcrosticheExistant(StringAAcrosticher);
		Map<Character, List<String>> lettre_acrostiches_MapAll = acrosticherNouveauMot(StringAAcrosticher);
		if (lettre_acrostiches_MapExistant.isEmpty()) {
			/*
			 * randomize la collection en local. Il n'existe pas d'acrostiche
			 * deja existant en base
			 */
			for (List<String> listMots : lettre_acrostiches_MapAll.values()) {
				Collections.shuffle(listMots);
			}
			return lettre_acrostiches_MapAll;
		} else {
			for (Entry<Character, List<String>> myEntryExistant : lettre_acrostiches_MapExistant
					.entrySet()) {
				List<String> myListMotExistant = lettre_acrostiches_MapExistant
						.get(myEntryExistant.getKey());
				List<String> myListMotAll = lettre_acrostiches_MapAll
						.get(myEntryExistant.getKey());
				for (String mot : myListMotAll) {
					if (!myListMotExistant.contains(mot)) {
						myListMotExistant.add(mot);
					}
				}
			}
			return lettre_acrostiches_MapExistant;
		}
	}

	@Override
	public boolean enregistrerAcroticher(String motAAcrosticher,
			Map<Character, List<String>> lettre_acrostiches_Map)
			throws SQLException {
		PreparedStatement insertIntoAcrostichesTable = null;
		PreparedStatement insertIntoAcrostice_MotTable = null;
		String insertIntoAcroticheString = "insert into ACROSTICHES(ID, motComplet) values (acro_seq.NEXTVAL, ?)";
		String insertIntoAcrostice_MotTableString = "insert into ACROTICHES_MOTS(IDAcrostiches, lettre, motLettre, position) values(?,?,?,?)";
		String cols[] = { "ID", "motComplet" };
		try {
			con.setAutoCommit(false);
			insertIntoAcrostichesTable = con.prepareStatement(
					insertIntoAcroticheString, cols);
			insertIntoAcrostichesTable.setString(1, motAAcrosticher);
			int affectedRows = insertIntoAcrostichesTable.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException(
						"Creating user failed, no rows affected.");
			}
			ResultSet generatedKeys = null;
			long insertKey = 0;
			generatedKeys = insertIntoAcrostichesTable.getGeneratedKeys();
			if (generatedKeys.next()) {
				insertKey = generatedKeys.getLong(1);
			} else {
				throw new SQLException(
						"Creating user failed, no generated key obtained.");
			}

			int position = motAAcrosticher.length();
			for (int i = position; i > 0; i--) {
				char key = motAAcrosticher.charAt(i - 1);
				int numberOfSameLetter = utils.StringUtils.countOccurrences(
						motAAcrosticher, key);
				String acrostiche = lettre_acrostiches_Map.get(key).get(
						numberOfSameLetter - 1);// les liste sont tri�s par
												// ordre d'occurence d'une
												// lettre dans un mot
				motAAcrosticher = motAAcrosticher.substring(0, i - 1);
				insertIntoAcrostice_MotTable = con
						.prepareStatement(insertIntoAcrostice_MotTableString);
				insertIntoAcrostice_MotTable.setLong(1, insertKey);
				insertIntoAcrostice_MotTable.setString(2,
						Character.toString(key));
				insertIntoAcrostice_MotTable.setString(3, acrostiche);
				insertIntoAcrostice_MotTable.setInt(4, i);
				insertIntoAcrostice_MotTable.executeUpdate();
				// con.commit();
			}
			con.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			if (con != null) {
				try {
					System.err.print("Transaction is being rolled back");
					con.rollback();
				} catch (SQLException excep) {
					e.printStackTrace();
				}
			}
		} finally {
			if (insertIntoAcrostichesTable != null) {
				insertIntoAcrostichesTable.close();
				insertIntoAcrostice_MotTable.close();
			}
			con.setAutoCommit(true);
		}
		return false;
	}
}
