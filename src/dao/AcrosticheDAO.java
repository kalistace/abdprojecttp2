package dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface AcrosticheDAO {

	Map<Character, List<String>> acrosticher(String text) throws SQLException;

	void changeHQ(String mot, int value) throws SQLException;

	boolean enregistrerAcroticher(String motAAcrosticher,
			Map<Character, List<String>> lettre_acrostiches_Map) throws SQLException;

}
