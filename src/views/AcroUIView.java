package views;

import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import models.AcroUIModel;

public class AcroUIView extends JPanel implements ChangeListener {
	private AcroUIModel model;


	public AcroUIView() {
		this.setPreferredSize(new Dimension(300, 200));
	}

	public void setModel(AcroUIModel model) {
		if (model == null)
			return;
		this.model = model;
		this.model.addChangeListener(this);
		this.repaint();
	}

	@Override
	public void stateChanged(ChangeEvent arg0) {
		repaint();
	}
}
