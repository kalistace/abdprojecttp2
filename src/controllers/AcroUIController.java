package controllers;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import models.AcroUIModel;
import dao.AcrosticheDAO;
import dao.AcrosticheDaoImpl;

public class AcroUIController extends JPanel implements ChangeListener {
	private AcrosticheDAO acrosticheDAO;
	private JPanel jpNorth;
	private JPanel jpSouth;
	private JTextField jtfMotAAcrosticher;
	private JButton jbAcrostischer;
	private JPanel jpAcrosticheGrid;
	private AcroUIModel model;
	private JPanel jpGridContainer;
	private JScrollPane jsrpAcrosticheCenter;
	private JButton jbEnregistrer;

	public AcroUIController() {
		try {
			acrosticheDAO = new AcrosticheDaoImpl();
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		this.setLayout(new BorderLayout());

		// HAUT DE l'IHM
		jpNorth = new JPanel();
		jpNorth.setLayout(new FlowLayout());
		jbAcrostischer = new JButton("Acrostischer !");

		ActionListener alAcrosticher = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Map<Character, List<String>> lettre_acrostiches_Map = null;
				try {
					lettre_acrostiches_Map = acrosticheDAO
							.acrosticher(jtfMotAAcrosticher.getText());
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				model.setMotAAcrosticher(jtfMotAAcrosticher.getText());
				model.setLettre_acrostiches_Map(lettre_acrostiches_Map);
			}
		};

		jbAcrostischer.addActionListener(alAcrosticher);
		jtfMotAAcrosticher = new JTextField();
		jtfMotAAcrosticher = new JTextField(25);
		jpNorth.add(jtfMotAAcrosticher);
		this.setBorder(BorderFactory.createTitledBorder("Acrosticher"));
		jpNorth.add(jbAcrostischer);
		this.add(jpNorth, BorderLayout.NORTH);

		// CENTRE DE l'IHM
		jpGridContainer = new JPanel(new FlowLayout());
		jpAcrosticheGrid = new JPanel(new GridLayout(0, 4));

		jpGridContainer.add(jpAcrosticheGrid);
		jsrpAcrosticheCenter = new JScrollPane(jpGridContainer);
		jsrpAcrosticheCenter
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		jsrpAcrosticheCenter.setMaximumSize(new Dimension(40, 40));
		this.add(jsrpAcrosticheCenter, BorderLayout.CENTER);

		// BAS de l'IHM
		jpSouth = new JPanel();
		jpSouth.setLayout(new FlowLayout());
		jbEnregistrer = new JButton("Enregistrer !");
		ActionListener alEnregistrer = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					acrosticheDAO.enregistrerAcroticher(
							model.getMotAAcrosticher(),
							model.getLettre_acrostiches_Map());
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		};
		jbEnregistrer.addActionListener(alEnregistrer);
		jpSouth.add(jbEnregistrer);
		this.add(jpSouth, BorderLayout.SOUTH);

	}

	public void setModel(AcroUIModel model) {
		if (model == null) {
			return;
		}
		this.model = model;
		this.model.addChangeListener(this);
		this.stateChanged(new ChangeEvent(this));
	}

	public void addAcrostiche(String acrostiche, char lettre) {
		final JLabel jlAcrostiche = new JLabel(acrostiche);
		JCheckBox jchboxHQ = new JCheckBox("HQ");
		jchboxHQ.setSelected(true);
		JButton jbAutre = new JButton("autre");
		this.jpAcrosticheGrid.add(jlAcrostiche);
		this.jpAcrosticheGrid.add(jchboxHQ);
		this.jpAcrosticheGrid.add(jbAutre);

		// fields cachés
		final JLabel jlLettre = new JLabel(Character.toString(lettre));
		jlLettre.setVisible(false);
		this.jpAcrosticheGrid.add(jlLettre);

		ActionListener alHQ = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				AbstractButton abstractButton = (AbstractButton) e.getSource();
				boolean isSelected = abstractButton.isSelected();
				if (isSelected) {
					try {
						acrosticheDAO.changeHQ(jlAcrostiche.getText(), 1);
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				} else {
					try {
						acrosticheDAO.changeHQ(jlAcrostiche.getText(), 0);
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				}
			}
		};

		jchboxHQ.addActionListener(alHQ);

		ActionListener alAutre = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				List<String> listeAutre = model.getAutres(jlLettre.getText()
						.charAt(0));

				if (listeAutre != null) {
					Collections.sort(listeAutre);
					JComboBox jcbAutres = new JComboBox();
					for (String mot : listeAutre) {
						jcbAutres.addItem(mot);
					}

					int response = JOptionPane.showConfirmDialog(
							AcroUIController.this, jcbAutres,
							"Selectionner un autre mot",
							JOptionPane.OK_CANCEL_OPTION,
							JOptionPane.QUESTION_MESSAGE);
					if (response == JOptionPane.OK_OPTION) {
						String selectedAutre = (String) jcbAutres
								.getSelectedItem();
						jlAcrostiche.setText(selectedAutre);
						model.notifyAutreSelected(jlLettre.getText(),
								selectedAutre, jlAcrostiche.getText());
						repaint();
					}
				} else {
					JOptionPane.showMessageDialog(AcroUIController.this,
							"Aucun autre mot possible !", "Avertissement !",
							JOptionPane.WARNING_MESSAGE);
				}
			}
		};

		jbAutre.addActionListener(alAutre);

	}

	@Override
	public void stateChanged(ChangeEvent e) {
		String motAAcrosticher = model.getMotAAcrosticher();
		this.jtfMotAAcrosticher.setText(model.getMotAAcrosticher());
		this.jpAcrosticheGrid.removeAll();

		List<String> lettre_acrostiches_Map = model.getAcrosticheAAfficher();
		if (!lettre_acrostiches_Map.isEmpty()) {
			/**
			 * @TODO A MODIFIER POUR NE FAIRE que les shuffle, etc etc dans le
			 *       model. S'adapter en fonction de ce que j'ai dans le DAO
			 *       enregistrer
			 */
			for (String mot : lettre_acrostiches_Map) {
				char key = mot.charAt(0);
				addAcrostiche(mot, key);
			}
		}
		revalidate();
		repaint();
	}

}
