import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;

import models.AcroUIModel;
import views.AcroUIView;
import controllers.AcroUIController;


public class PgmAcroUI extends JFrame {
		
	public PgmAcroUI(){
		JPanel jpGauche= new JPanel();
		jpGauche.setLayout(new BorderLayout());
		AcroUIModel model = new AcroUIModel();
		this.setTitle("My Acrotische Program");
		
		AcroUIController charctrl = new AcroUIController();
		charctrl.setModel(model);
		jpGauche.add(charctrl, BorderLayout.CENTER);
		
		AcroUIView vue = new AcroUIView();
		vue.setModel(model);
		this.setSize(800, 600);
		this.add(jpGauche);
		
/*		for (int i = 0; i < 100; i++) {
			charctrl.addAcrostiche("test", 't', i);
		}*/
		
	}
	
	public static void main(String args[]){
		PgmAcroUI pgChart= new PgmAcroUI();
		pgChart.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		//pgChart.pack();
		pgChart.setLocationRelativeTo(null);
		pgChart.setVisible( true );
	}

}
